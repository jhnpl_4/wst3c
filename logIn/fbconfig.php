<?php  
require_once "vendor/autoload.php";

session_start();

$fb=new \Facebook\Facebook([
    'app_id'=>'668110167746567',
    'app_secret'=>'c2838f92ec4d793eaeba0af20a12737e',
    'default_graph_version'=>'v2.5',
]);
$helper=$fb->getRedirectLoginHelper();

    if(isset($_GET['code'])){
    if(isset($_SESSION['access_token'])){
        $access_token=$_SESSION['access_token'];
    }else
    {
        $access_token=$helper->getAccessToken();
        $_SESSION['access_token']=$access_token;

        $fb->setDefaultAccessToken($_SESSION['access_token']);
    }
    $_SESSION['user_name']='';
    $_SESSION['user_email_address']='';

    $graph_response=$fb->get("/me?fields=name,email",$access_token);
    $facebook_user_info=$graph_response->getGraphUser();

    $_SESSION['user_name']=$facebook_user_info['name'];
    $_SESSION['user_email_address']=$facebook_user_info['email'];

    }else
    {
    $permissions=['email'];

    $login_url=$helper->getLoginUrl('http://localhost/projects/wst3c/wst3c/logIn/',$permissions);
}
?>