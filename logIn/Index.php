<?php include "fbconfig.php";?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>WebLogin</title>
	<link rel="stylesheet" type="text/css" href="bootstrap-5.1.3-dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="bootstrap-5.1.3-dist/js/bootstrap.min.js"></link>
	<script type="text/javascript" src="bootstrap-5.1.3-dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
	<style>
        body{
            background: #7C7C7C;
            display: grid;
            height: 100vh;
            flex-direction: column;
        }
        input{
            border: 2px solid #ccc;
            padding: 0px 20px;   
        }
        button{
            float: center;
            background: #FFFFFF;
            padding: 10px 15px;
            color: #fff;
            border-radius: 5px;
        }
        button:hover{
            opacity: .7;
        }
        h2 {
            text-align: center;
            color: black;
        }
</style>

	<div class="container">
		<br><br>

		<?php 
			if(isset($login_url)){

				include "form.php";	
			}
			else
			{

				echo '<div class="card card-signin my-5"><div class="card-body">';
				echo '<center><div class="for-label-group">
						<table class="w3-table">
							<tr>
							  <th>Name:</th>
							  <th>Date:</th>
							</tr>
							<tr>
							  <td><input type="text" value="John Paul C. Pimentel" name="name" readonly></td>
							  <td><input type="text" id="currentDate" value="2022/03/04"></td>
							</tr>
						</table>
					</div></center>
					<center><div class="for-label-group">
						<table class="w3-table">
							<tr>
							  <th>Year and Section:</th>
							  <th>Time:</th>
							</tr>
							<tr>
							  <td><input type="text" value="3C" name="sec" readonly></td><br>
							  <td><input type="text" id="currentTime" value="9:09"></td>
							</tr>
						</table>
					</div></center><br>';
				echo '<center><h4> Welcome User,</h4></center>';

				echo '<center><h3><b>FB Name: </b>:'.$_SESSION['user_name'].'</h3></center>';
				echo '<center><h3><b>Email Address: </b>:'.$_SESSION['user_email_address'].'</h3></center>';

				echo '<center><h5><button><a href="logout.php">Logout</a></button></h5>';
				echo '</div></div>';
			}
		?>
	</div>
</body>
</html>