<div class="container">
	<div class="row">
		<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
			<div class="card-body">
				<form class="form-signin">
					<div class="for-label-group">
						<table class="w3-table">
							<tr>
							  <th>Name:</th>
							  <th>Date:</th>
							</tr>
							<tr>
							  <td><input type="text" value="John Paul C. Pimentel" name="name" readonly></td>
							  <td><input type="text" id="currentDate">
				               <script>
				                 var today = new Date();
				                 var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
				                 document.getElementById("currentDate").value = date;
				               </script></td>
							</tr>
						</table>
					</div>
					<div class="for-label-group">
						<table class="w3-table">
							<tr>
							  <th>Year and Section:</th>
							  <th>Time:</th>
							</tr>
							<tr>
							  <td><input type="text" value="3C" name="sec" readonly></td><br>
							  <td><input type="text" id="currentTime">
				               <script>
				                 var today = new Date();
				               var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
				                 document.getElementById("currentTime").value = time;
				               </script></td>
							</tr>
						</table>
					</div><br>
					<h5 class="card-title etxt-center">Sign In</h5>
					<div class="for-label-group">
						<input type="email" id="inputEmail" class="form-control" placeholder="Email">
						<label for="inputEmail">Email address</label>	
					</div>
					<div class="for-label-group">
						<input type="password" id="inputPassword" class="form-control" placeholder="password">
						<label for="inputPassword">Password</label>	
					</div>
					<div class="custom-control custom-checkbox mb-3">
						<input type="checkbox" id="customCheck1" class="custom-control-input">
						<label class="custom-control-label" for="customCheck1">Remember Password</label>	
					</div>
					<center><button class="btn-lg btn-primary btn-block text-uppercase" type="submit">Sign in</button></center>

					<hr class="my-4">
					<center><button><a href="<?php echo $login_url;?>" class="btn-lg btn-facebook btn-block "><i class="fab fa-facebook-f mr-2"></i>Log in with Facebook</a></button></center><br>

					<center><button><a href="<?php echo $login_url;?>" class="btn-lg btn-facebook btn-block "><i class="fab fa-facebook-f mr-2"></i>Log in with Google</a></button></center><br>
				
				</form>
			</div>
			
		</div>
	</div>
	
</div>