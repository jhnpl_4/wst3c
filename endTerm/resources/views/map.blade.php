<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- basic -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>Lotus Grace Farm</title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- bootstrap css -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <!-- style css -->
      <link rel="stylesheet" href="css/style.css">
      <!-- Responsive-->
      <link rel="stylesheet" href="css/responsive.css">
      <!-- Favicon-->
        <link rel="icon" href="images/log.png">
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
      <!-- Tweaks for older IEs-->
      <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <style>
            .top {
              position: fixed;
              bottom: 10px;
              right: 10px;
              max-width: 50px;
              max-height: 50px;
              width: 100%;
              height: 100%;
              padding: .5px;
              border-radius: 8px;
              justify-content: center;
              background: #54c9a7;
              font-size: 1rem;
              font-weight: 800;
              text-decoration: none;
              cursor: pointer;
            }
nav {
            background: #54c9a7;
            color: #fff;
            display: flex;
            justify-content: space-between;
        }
        
        nav .mainMenu {
            display: flex;
            list-style: none;
        }
        
        nav .mainMenu li a {
            display: inline-block;
            padding: 15px;
            text-decoration: none;
            text-transform: uppercase;
            color: #fff;
            font-size: 1.5rem;
        }
        
        nav .mainMenu li a:hover {
            background: #000000;
        }
        
        nav .openMenu {
            font-size: 2rem;
            margin: 20px;
            display: none;
            cursor: pointer;
        }
        
        nav .mainMenu .closeMenu,
        .icons i {
            font-size: 2rem;
            display: none;
            cursor: pointer;
        }
        
        nav .logo {
            margin: 6px;
            font-size: 25px;
            cursor: pointer;
        }
        
        @media (max-width: 800px) {
            nav .mainMenu {
                height: 100vh;
                position: fixed;
                top: 0;
                right: 0;
                left: 0;
                z-index: 10;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                background: #54c9a7;
                transition: top 1s ease;
                display: none;
            }
            nav .mainMenu .closeMenu {
                display: block;
                position: absolute;
                top: 20px;
                right: 20px;
            }
            nav .openMenu {
                display: block;
            }
            nav .mainMenu li a:hover {
                background: #000000;
                /*color: rgb(255, 123, 0);*/
                font-size: 1.6rem;
            }
</style>
</head>
   <!-- body -->
   <body class="main-layout">
      <!-- nav -->
       <nav id="top">
        <div class="logo">
            <a href="/home"><img src="images/log.png" width="190" 
     height="190" alt=""></a>
        </div>
        <div class="openMenu"><i class="fa fa-bars"></i></div>
        <ul class="mainMenu">
            <li class="active"> <a href="/home" style="margin-top: 17px;">Go Back</a></li>
            <div class="closeMenu"><i class="fa fa-times"></i></div>
        </ul>
    </nav>
      <!-- end nav -->
      <div class="container-xxl courses py-6" style="margin-top: 40px; margin-right: -300px;">
        <div class="container">
            <div class="row g-5">
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s" style="min-height: 450px;">
                    <div class="position-relative h-100">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5681.945779158288!2d120.78950649!3d16.036172416077502!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33911937bc9372f9%3A0x2cac11bb86f8e45e!2sSan%20Maximo%2C%20Natividad%2C%20Pangasinan!5e1!3m2!1sen!2sph!4v1655623757683!5m2!1sen!2sph" width="800" height="500" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
      <!-- Javascript files--> 
      <script src="js/jquery.min.js"></script> 
      <script src="js/popper.min.js"></script> 
      <script src="js/bootstrap.bundle.min.js"></script> 
      <script src="js/jquery-3.0.0.min.js"></script> 
      <script src="js/plugin.js"></script> 
      <!-- sidebar --> 
      <script src="js/jquery.mCustomScrollbar.concat.min.js"></script> 
      <script src="js/custom.js"></script>
      <script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
      <script>
         $(document).ready(function(){
         $(".fancybox").fancybox({
         openEffect: "none",
         closeEffect: "none"
         });
         
         $(".zoom").hover(function(){
         
         $(this).addClass('transition');
         }, function(){
         
         $(this).removeClass('transition');
         });
         });
         
      </script> 
   </body>
</html>