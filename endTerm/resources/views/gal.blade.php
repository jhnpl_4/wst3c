<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<style>
      body{
   background: #9CEED8;
   display: flex;
   justify-content: center;
   align-items: center;
   height: 130vh;
   flex-direction: column;
}
button{
   float: right;
   background: #2C987B;
   padding: 10px 15px;
   color: #ffffff;
   border-radius: 5px;
   margin-right: 10px;
}

button:hover{
   background: #51DFB9;
   color: #000000;
}
img {
  filter: gray; /* IE6-9 */
  -webkit-filter: grayscale(1); /* Google Chrome, Safari 6+ & Opera 15+ */
    -webkit-box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
    box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
    margin-bottom:20px;
}

img:hover {
  filter: none; /* IE6-9 */
  -webkit-filter: grayscale(0); /* Google Chrome, Safari 6+ & Opera 15+ */
 
}
h1{
   color: #2C987B;
}
</style>
<body>
    <h1>Gallery</h1><br>
<a href="/home"></h2>
<div class="container">
            
        <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/1.jpg"/></div>
        <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/2.jpg"/></div>
        <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/3.jpg"/></div>
      <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/4.jpg"/></div>
      <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/5.jpg"/></div>
        <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/6.jpg"/></div>
        <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/7.jpg"/></div>
      <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/8.jpg"/></div>
      <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/9.jpg"/></div>
      <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/10.jpg"/></div>
     </div>
</body>
</html>