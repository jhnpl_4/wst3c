<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<style>
      body{
   background: #9CEED8;
   display: flex;
   justify-content: center;
   align-items: center;
   height: 130vh;
   flex-direction: column;
}
button{
   float: right;
   background: #2C987B;
   padding: 10px 15px;
   color: #ffffff;
   border-radius: 5px;
   margin-right: 10px;
}

button:hover{
   background: #51DFB9;
   color: #000000;
}
img {
  filter: gray; /* IE6-9 */
  -webkit-filter: grayscale(1); /* Google Chrome, Safari 6+ & Opera 15+ */
    -webkit-box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
    box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
    margin-bottom:20px;
}

img:hover {
  filter: none; /* IE6-9 */
  -webkit-filter: grayscale(0); /* Google Chrome, Safari 6+ & Opera 15+ */
 
}
   </style>
<body>
<div class="container">
            <h2><button><a href="/home">Back</button></h2><br>
        <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/a.jpg"/></div>
        <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/b.jpg"/></div>
        <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/c.jpg"/></div>
      <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/d.jpg"/></div>
      <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/e.jpg"/></div>
        <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/f.jpg"/></div>
        <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/g.jpg"/></div>
      <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/h.jpg"/></div>
      <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/i.jpg"/></div>
      <div class="col-md-3 col-sm-4 col-xs-6"><img class="img-responsive" src="gallery/j.jpg"/></div>
     </div>
</body>
</html>