<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- basic -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>Lotus Grace Farm</title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- bootstrap css -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <!-- style css -->
      <link rel="stylesheet" href="css/style.css">
      <!-- Responsive-->
      <link rel="stylesheet" href="css/responsive.css">
      <!-- fevicon -->
      <link rel="icon" href="images/fevicon.png" type="image/gif" />
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
      <!-- Tweaks for older IEs-->
      <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <style>
            .top {
              position: fixed;
              bottom: 10px;
              right: 10px;
              max-width: 50px;
              max-height: 50px;
              width: 100%;
              height: 100%;
              padding: .5px;
              border-radius: 8px;
              justify-content: center;
              background: #54c9a7;
              font-size: 1rem;
              font-weight: 800;
              text-decoration: none;
              cursor: pointer;
            }
            h2{
               color: white;
            }            
        p{
            font-size: 10px;
        }
        table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
color: black;
}

td, th {
  border: 1px solid #54c9a7;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #000000;
}
</style>
</head>
   <!-- body -->
   <body class="main-layout">
      <!-- header -->
      <header class="header" id="top">
         <!-- header inner -->
         <div class="header">
         <div class="container">
            <div class="row">
               <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col logo_section">
                  <div class="full">
                     <div class="center-desk">
                        <div class="logo"> <a href="index.html">Lotus Grace</a> </div>
                     </div>
                  </div>
               </div>
               <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                  <div class="menu-area">
                     <div class="limit-box">
                        <nav class="main-menu">
                           <ul class="menu-area-main">
                              <li class="active"> <a href="index.html">Home</a> </li>
                              <li> <a href="#about">About</a> </li>
                              <li> <a href="#service">Features</a> </li>
                              <li> <a href="#download">Others</a> </li>
                              <li> <a href="/login">Login</a> </li>
                           </ul>
                        </nav>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end header inner --> 
      </header>
      <!-- end header -->
      <section class="slider_section">
         <div id="myCarousel" class="carousel slide banner-main" data-ride="carousel">
            <div class="carousel-inner">
               <div class="carousel-item active">
                  <img class="first-slide" src="images/banner.jpg" alt="First slide">
                  <div class="container">
                     <div class="carousel-caption relative">
                        <h1>Welcome to Lotus Grace Farm</h1>
                        <h2>Lotus Beautiful Heavenly Grace</h2>
                        <a  href="#about">Read More</a>
                     </div>
                  </div>
               </div>
               <div class="carousel-item">
                  <img class="second-slide" src="images/banner2.jpg" alt="Second slide">
                  <div class="container">
                     <div class="carousel-caption relative">
                        <h1>Welcome to Lotus Grace Farm</h1>
                        <h2>Lotus Beautiful Heavenly Grace</h2>
                        <a  href="#about">Read More</a>
                     </div>
                  </div>
               </div>
               <div class="carousel-item">
                  <img class="third-slide" src="images/banner3.jpg" alt="Third slide">
                  <div class="container">
                     <div class="carousel-caption relative">
                        <h1>Welcome to Lotus Grace Farm</h1>
                        <h2>Lotus Beautiful Heavenly Grace</h2>
                        <a  href="#about">Read More</a>
                     </div>
                  </div>
               </div>
            </div>
            <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <i class='fa fa-angle-left'></i>
            </a>
            <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <i class='fa fa-angle-right'></i>
            </a>
         </div>
      </section>
      <!-- about  -->
      <div id="about" class="about top_layer">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="titlepage">
                     <h2>About Informations</h2>
                     <div class="section" id="skill">
                          <div class="container">
                            <div class="card" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                              <div class="card-body">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="progress-container progress-primary"><span class="progress-badge">Cleanliness</span>
                                      <div class="progress">
                                        <div class="progress-bar progress-bar-primary" data-aos="progress-full" data-aos-offset="10" data-aos-duration="2000" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 95%; background: #54c9a7;"></div><span class="progress-value"></span><p>95%</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="progress-container progress-primary"><span class="progress-badge">Location</span>
                                      <div class="progress">
                                        <div class="progress-bar progress-bar-primary" data-aos="progress-full" data-aos-offset="10" data-aos-duration="2000" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 93%; background: #54c9a7;"></div><span class="progress-value"></span><p>93%</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="progress-container progress-primary"><span class="progress-badge">Service</span>
                                      <div class="progress">
                                        <div class="progress-bar progress-bar-primary" data-aos="progress-full" data-aos-offset="10" data-aos-duration="2000" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 99%; background: #54c9a7;"></div><span class="progress-value"></span><p>99%</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="progress-container progress-primary"><span class="progress-badge">Facilities</span>
                                      <div class="progress">
                                        <div class="progress-bar progress-bar-primary" data-aos="progress-full" data-aos-offset="10" data-aos-duration="2000" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 95%; background: #54c9a7;"></div><span class="progress-value"></span><p>95%</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="progress-container progress-primary"><span class="progress-badge">Value for money</span>
                                      <div class="progress">
                                        <div class="progress-bar progress-bar-primary" data-aos="progress-full" data-aos-offset="10" data-aos-duration="2000" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 95%; background: #54c9a7;"></div><span class="progress-value"></span><p>95%</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <div class="img-box">
                     <figure><img src="images/abt.png" alt="img"/></figure>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- end abouts -->
      <!-- service --> 
      <div id="service" class="service">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="titlepage">
                     <h2>Services </h2>
                     <span>Our finest services have brought joy and smile to every person, family, and friends who visits us<br>Enjoy and seize the unforgetable experience with us</span>
                  </div>
               </div>
            </div>
         </div>
         <div class="container margin-r-l">
            <div class="row">
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 thumb">
                  <div class="service-box">
                     <figure>
                        <a href="" class="fancybox" rel="ligthbox">
                        <img  src="" class="zoom img-fluid "  alt="">
                        </a>
                        <span class="hoverle">
                        <a href="images/11.jpg" class="fancybox" rel="ligthbox">Catering</a>
                        </span>  
                     </figure>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 thumb">
                  <div class="service-box">
                     <figure>
                        <a href="" class="fancybox" rel="ligthbox">
                        <img  src="" class="zoom img-fluid "  alt="">
                        </a>
                        <span class="hoverle">
                        <a href="images/44.jpg" class="fancybox" rel="ligthbox">Pond</a>
                        </span>
                     </figure>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 thumb">
                  <div class="service-box">
                     <figure>
                        <a href="" class="fancybox" rel="ligthbox">
                        <img  src="" class="zoom img-fluid "  alt="">
                        </a>
                        <span class="hoverle">
                        <a href="images/33.jpg" class="fancybox" rel="ligthbox">Pool</a>
                        </span>
                     </figure>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 thumb">
                  <div class="service-box">
                     <figure>
                        <a href="" class="fancybox" rel="ligthbox">
                        <img  src="" class="zoom img-fluid "  alt="">
                        </a>
                        <span class="hoverle">
                        <a href="images/22.jpg" class="fancybox" rel="ligthbox">Venue</a>
                        </span> 
                     </figure>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 thumb">
                  <div class="service-box">
                     <figure>
                        <a href="" class="fancybox" rel="ligthbox">
                        <img  src="" class="zoom img-fluid "  alt="">
                        </a>
                        <span class="hoverle">
                        <a href="images/55.jpg" class="fancybox" rel="ligthbox">Foods</a>
                        </span> 
                     </figure>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- end service -->
      <!-- Download -->
      <div id="download" class="download">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="titlepage">
                     <h2>Other Services</h2>
                     <span>Our finest services have brought joy and smile to every person, family, and friends who visits us<br>Enjoy and seize the unforgetable experience with us</span>
                  </div>
               </div>
            </div>
            
                    <table border="3" align="center" class="table table-striped">
  <tr>
   <th>No</th>
    <th>Feautures</th>
    <th>Details</th>
    <th>Image</th>
  </tr>
  <tbody>
  <tr>
   @if(count($input) > 0 )
   @foreach($input as $prod)
   <th scope="row">1</th>
    <td>{{ $prod->name }}</td>
    <td>{{ $prod->detail }}</td>
    <td><img class="w-50" src="{{ url('/image/'.$prod->image) }}" alt=""></td>
      </tr>
   </tbody>

   @endforeach
   @endif
</table>
         </div>
      </div>
      <!-- end Download -->
      <!--  footer --> 
      <footr>
         <div class="footer">
            <div class="container">
               <div class="row">
                  <div class="col-lg-2 col-md-6 col-sm-12 width">
                     <div class="address">
                        <h3>Address</h3>
                        <i><img src="icon/3.png">San Maximo, Natividad, Pangasinan</i>
                     </div>
                  </div>
                  <div class="col-lg-2 col-md-6 col-sm-12 width">
                     <div class="address">
                        <h3>Social Media </h3>
                        <ul class="contant_icon">
                           <li><a href="https://www.facebook.com/lotusgracefarm/"><img src="icon/fb.png" alt="icon"/></a></li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-2 col-md-6 col-sm-12 width">
                     <div class="address">
                        <h3>Phone Number</h3>
                        <i><img src="icon/no.png">0949 644 4055</i>
                     </div>
                  </div>
                  <div class="container-top">
                        <a href="#top"><button class="top">↑</button></a>
                    </div>
            </div>
            <div class="copyright">
               <p>Copyright 2022 All Right Reserved</p>
            </div>
         </div>
      </footer>
      <!-- end footer -->
      <!-- Javascript files--> 
      <script src="js/jquery.min.js"></script> 
      <script src="js/popper.min.js"></script> 
      <script src="js/bootstrap.bundle.min.js"></script> 
      <script src="js/jquery-3.0.0.min.js"></script> 
      <script src="js/plugin.js"></script> 
      <!-- sidebar --> 
      <script src="js/jquery.mCustomScrollbar.concat.min.js"></script> 
      <script src="js/custom.js"></script>
      <script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
      <script>
         $(document).ready(function(){
         $(".fancybox").fancybox({
         openEffect: "none",
         closeEffect: "none"
         });
         
         $(".zoom").hover(function(){
         
         $(this).addClass('transition');
         }, function(){
         
         $(this).removeClass('transition');
         });
         });
         
      </script> 
   </body>
</html>