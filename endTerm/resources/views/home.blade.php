<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- basic -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>Lotus Grace Farm</title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- bootstrap css -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <!-- style css -->
      <link rel="stylesheet" href="css/style.css">
      <!-- Responsive-->
      <link rel="stylesheet" href="css/responsive.css">
      <!-- Favicon-->
        <link rel="icon" href="images/log.png">
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
      <!-- Tweaks for older IEs-->
      <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <style>
            .top {
              position: fixed;
              bottom: 10px;
              right: 10px;
              max-width: 50px;
              max-height: 50px;
              width: 100%;
              height: 100%;
              padding: .5px;
              border-radius: 8px;
              justify-content: center;
              background: #54c9a7;
              font-size: 1rem;
              font-weight: 800;
              text-decoration: none;
              cursor: pointer;
            }
            h2{
               color: white;
            }            
        p{
            font-size: 18px;
        }
        table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: center;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #D6EEEE;
  align-items: center;
}

.center {
text-align: center;

}
nav {
            background: #54c9a7;
            color: #fff;
            display: flex;
            justify-content: space-between;
        }
        
        nav .mainMenu {
            display: flex;
            list-style: none;
        }
        
        nav .mainMenu li a {
            display: inline-block;
            padding: 15px;
            text-decoration: none;
            text-transform: uppercase;
            color: #fff;
            font-size: 1.5rem;
        }
        
        nav .mainMenu li a:hover {
            background: #000000;
        }
        
        nav .openMenu {
            font-size: 2rem;
            margin: 20px;
            display: none;
            cursor: pointer;
        }
        
        nav .mainMenu .closeMenu,
        .icons i {
            font-size: 2rem;
            display: none;
            cursor: pointer;
        }
        
        nav .logo {
            margin: 6px;
            font-size: 25px;
            cursor: pointer;
        }
        
        @media (max-width: 800px) {
            nav .mainMenu {
                height: 100vh;
                position: fixed;
                top: 0;
                right: 0;
                left: 0;
                z-index: 10;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                background: #54c9a7;
                transition: top 1s ease;
                display: none;
            }
            nav .mainMenu .closeMenu {
                display: block;
                position: absolute;
                top: 20px;
                right: 20px;
            }
            nav .openMenu {
                display: block;
            }
            nav .mainMenu li a:hover {
                background: #000000;
                /*color: rgb(255, 123, 0);*/
                font-size: 1.6rem;
            }
            #active{
               color: black;
            }
            footer{
                background-color:#54c9a7;
            }
.copyright-menu ul {
  text-align: right;
  margin: 0; }

.copyright-menu li {
  display: inline-block;
  padding-left: 20px; }
</style>
</head>
   <!-- body -->
   <body class="main-layout">
      <!-- nav -->
       <nav id="top">
        <div class="logo">
            <a href="/home"><img src="images/log.png" width="190" 
     height="190" alt=""></a>
        </div>
        <div class="openMenu"><i class="fa fa-bars"></i></div>
        <ul class="mainMenu" style="margin-top: 17px;">
            <li> <a href="/home" class="active font-weight-bold" style="color: black;">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#service">Features</a></li>
            <li><a href="#download">Others</a></li>
            <li><a href="/gal">Gallery</a></li>
            <div class="closeMenu"><i class="fa fa-times"></i></div>
        </ul>
    </nav>
      <!-- end nav -->
      <section class="slider_section">
         <div id="myCarousel" class="carousel slide banner-main" data-ride="carousel">
            <div class="carousel-inner">
               <div class="carousel-item active">
                  <img class="first-slide" src="images/12.jpg" alt="First slide">
                  <div class="container">
                     <div class="carousel-caption relative">
                        <h1>Welcome to Lotus Grace Farm</h1>
                        <h2>Lotus Beautiful Heavenly Grace</h2>
                        <a  href="#about">Read More</a>
                     </div>
                  </div>
               </div>
               <div class="carousel-item">
                  <img class="second-slide" src="images/13.jpg" alt="Second slide">
                  <div class="container">
                     <div class="carousel-caption relative">
                        <h1>Welcome to Lotus Grace Farm</h1>
                        <h2>Lotus Beautiful Heavenly Grace</h2>
                        <a  href="#about">Read More</a>
                     </div>
                  </div>
               </div>
               <div class="carousel-item">
                  <img class="third-slide" src="images/14.jpg" alt="Third slide">
                  <div class="container">
                     <div class="carousel-caption relative">
                        <h1>Welcome to Lotus Grace Farm</h1>
                        <h2>Lotus Beautiful Heavenly Grace</h2>
                        <a  href="#about">Read More</a>
                     </div>
                  </div>
               </div>
            </div>
            <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <i class='fa fa-angle-left'></i>
            </a>
            <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <i class='fa fa-angle-right'></i>
            </a>
         </div>
      </section>
      <!-- about  -->
      <div id="about" class="about top_layer">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="titlepage">
                     <h2>About Lotus Grace</h2>
                     <div class="section" id="skill">
                          <div class="container">
                            <div class="card" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                              <div class="card-body">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="progress-container progress-primary"><span class="progress-badge">Cleanliness</span>
                                      <div class="progress">
                                        <div class="progress-bar progress-bar-primary" data-aos="progress-full" data-aos-offset="10" data-aos-duration="2000" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 95%; background: #54c9a7;"></div><span class="progress-value"></span>
                                      </div><p>95%</p><br>
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="progress-container progress-primary"><span class="progress-badge">Location</span>
                                      <div class="progress">
                                        <div class="progress-bar progress-bar-primary" data-aos="progress-full" data-aos-offset="10" data-aos-duration="2000" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 93%; background: #54c9a7;"></div><span class="progress-value"></span>
                                      </div><p>93%</p><br>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="progress-container progress-primary"><span class="progress-badge">Service</span>
                                      <div class="progress">
                                        <div class="progress-bar progress-bar-primary" data-aos="progress-full" data-aos-offset="10" data-aos-duration="2000" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 99%; background: #54c9a7;"></div><span class="progress-value"></span>
                                      </div><p>99%</p><br>
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="progress-container progress-primary"><span class="progress-badge">Facilities</span>
                                      <div class="progress">
                                        <div class="progress-bar progress-bar-primary" data-aos="progress-full" data-aos-offset="10" data-aos-duration="2000" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 95%; background: #54c9a7;"></div><span class="progress-value"></span>
                                      </div><p>95%</p><br>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="progress-container progress-primary"><span class="progress-badge">Value for money</span>
                                      <div class="progress">
                                        <div class="progress-bar progress-bar-primary" data-aos="progress-full" data-aos-offset="10" data-aos-duration="2000" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 95%; background: #54c9a7;"></div><span class="progress-value"></span>
                                      </div><p>95%</p><br>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                  </div>
               </div>
            </div>
            <div class="center">
                  <h3>(name)</h3>
                  <span><i class="fa fa-quote-left"></i> ( owner )<i class="fa fa-quote-right"></i></span>
                  <p>ehufsbgejfiongnfsjbjiojiwhejgknsfjbirghgbnrsj</p>
            </div><br>
         </div>
      </div>
      <!-- end abouts -->
      <!-- service --> 
      <div id="service" class="service">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="titlepage">
                     <h2>Main Services </h2>
                     <span>Our finest services have brought joy and smile to every person, family, and friends who visits us<br>Enjoy and seize the unforgetable experience with us</span>
                  </div>
               </div>
            </div>
         </div>
         <div class="container margin-r-l">
            <div class="row">
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 thumb">
                  <div class="service-box">
                     <figure>
                        <a href="images/1.jpg" class="fancybox" rel="ligthbox">
                        <img  src="images/1.jpg" class="zoom img-fluid "  alt="">
                        </a>
                        <span class="hoverle">
                        <a href="images/6.jpg" class="fancybox" rel="ligthbox">Catering</a>
                        </span>  
                     </figure>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 thumb">
                  <div class="service-box">
                     <figure>
                       <a href="images/4.jpg" class="fancybox" rel="ligthbox">
                        <img  src="images/4.jpg" class="zoom img-fluid "  alt="">
                        </a>
                        <span class="hoverle">
                        <a href="images/9.jpg" class="fancybox" rel="ligthbox">Pond</a>
                        </span>
                     </figure>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 thumb">
                  <div class="service-box">
                     <figure>
                        <a href="images/3.jpg" class="fancybox" rel="ligthbox">
                        <img  src="images/3.jpg" class="zoom img-fluid "  alt="">
                        </a>
                        <span class="hoverle">
                        <a href="images/8.jpg" class="fancybox" rel="ligthbox">Pool</a>
                        </span>
                     </figure>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 thumb">
                  <div class="service-box">
                     <figure>
                        <a href="images/2.jpg" class="fancybox" rel="ligthbox">
                        <img  src="images/2.jpg" class="zoom img-fluid "  alt="">
                        </a>
                        <span class="hoverle">
                        <a href="images/7.jpg" class="fancybox" rel="ligthbox">Venue</a>
                        </span> 
                     </figure>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 thumb">
                  <div class="service-box">
                     <figure>
                        <a href="images/5.jpg" class="fancybox" rel="ligthbox">
                        <img  src="images/5.jpg" class="zoom img-fluid "  alt="">
                        </a>
                        <span class="hoverle">
                        <a href="images/0.jpg" class="fancybox" rel="ligthbox">Foods</a>
                        </span> 
                     </figure>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- end service -->
      <!-- Download -->
      <div id="download" class="download">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="titlepage">
                     <h2>Other Services</h2>
                     <span>Our finest services have brought joy and smile to every person, family, and friends who visits us<br>Enjoy and seize the unforgetable experience with us</span>
                  </div>
               </div>
            </div>
            
                    <table border="3" align="center" class="table table-striped">
  <tr>
    <th>Feautures</th>
    <th>Details</th>
    <th>Image</th>
      </tr>
  <tbody>
  <tr>
   @if(count($input) > 0 )
   @foreach($input as $prod)
    <td>{{ $prod->name }}</td>
    <td>{{ $prod->detail }}</td>
    <td><img class="w-50" src="{{ url('/image/'.$prod->image) }}" alt=""></td>
      </tr>
   </tbody>

   @endforeach
   @endif
</table>
         </div>
      </div>
      <!-- end Download -->
      <!--  footer -->
<footer>
    <div class="container-top">
                        <a href="#top"><button class="top">↑</button></a>
                    </div>
  <div class="container-fluid copyright text-light py-4 wow fadeIn" style=" background-color:#54c9a7;" data-wow-delay="0.1s">
            <div class="container">
                <div class="row">
                    <div class=" text-center mb-3 mb-md-0">
                <p style="color:white; font-size: 16px;">Copyright &copy; 2022 All Rights Reserved by 
             <a href="/">Lotus Grace Farm</a></p>
                    </div>
                    <!-- End Col -->
                    <div class="col-md-6">
                        <div class="copyright-menu">
                            <ul>
                                <li>
                                    <p><strong>Give us a call</strong></p>
                                    <p>0949 644 4055</p>
                                </li><br>
                                <li>
                                    <p><strong>Social Media</strong></p>
                                    <p><a href="https://www.facebook.com/lotusgracefarm/"><img src="icon/fb.png" alt="icon"/></a></p>
                                </li><br>
                                <li>
                                    <p><strong>Address</strong></p>
                                    <p><a href="/map"><img src="icon/3.png" alt="icon"/>San Maximo Natividad, Pangasinan</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="container-top">
                        <a href="#top"><button class="top">↑</button></a>
                    </div>
        </div>
</footer>
      <!-- end footer -->
      <!-- Javascript files--> 
      <script src="js/jquery.min.js"></script> 
      <script src="js/popper.min.js"></script> 
      <script src="js/bootstrap.bundle.min.js"></script> 
      <script src="js/jquery-3.0.0.min.js"></script> 
      <script src="js/plugin.js"></script> 
      <!-- sidebar --> 
      <script src="js/jquery.mCustomScrollbar.concat.min.js"></script> 
      <script src="js/custom.js"></script>
      <script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
      <script>
         $(document).ready(function(){
         $(".fancybox").fancybox({
         openEffect: "none",
         closeEffect: "none"
         });
         
         $(".zoom").hover(function(){
         
         $(this).addClass('transition');
         }, function(){
         
         $(this).removeClass('transition');
         });
         });
         
      </script> 
   </body>
</html>