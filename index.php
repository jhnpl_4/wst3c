<!DOCTYPE html>
<html>
<head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="bootstrap-5.1.3-dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
   <style>
      body{
                   background: #7C7C7C;
                   display: grid;
                   justify-content: center;
                   align-items: center;
                   height: 100vh;
                   flex-direction: column;
                }
                form{
                   width: 500px;
                   height: 260px;
                   border: 2px solid #ccc;
                   padding: 30px;
                   background: #E5E5E5;
                   border-radius: 15px;
                }
                button{
                   float: right;
                   background: #555;
                   padding: 10px 15px;
                   color: #fff;
                   border-radius: 5px;
                   margin-right: 350px;
                }
                input{
                   border: 2px solid #ccc;
                   margin-left: 60px;
                   padding: 0px 20px;   
                }
                button:hover{
                   opacity: .7;
                }
                h2 {
                   text-align: center;
                   color: black;
                }
                label{
                  margin-left: 50px;
                }
                
</style>
<form action = "" method = "GET">
        <h2>Information</h2><br>
                <label>Name:</label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                <input type="text" value="John Paul C. Pimentel" name="name" readonly><br><br>
                <label>Year and Section:</label>
                <input type="text" value="3C" name="sec" readonly><br><br>
                <label>Subject:</label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                <input type="text" value="Elective 1 (Web Systems and Technologies 2)" name="sub" readonly><br><br>
                <label>Date:</label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
               <input type="text" id="currentDate">
                
               <script>
                 var today = new Date();
                 var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                 document.getElementById("currentDate").value = date;
               </script><br><br>

               <label>Time:</label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
               <input type="text" id="currentTime">
               <script>
                 var today = new Date();
               var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                 document.getElementById("currentTime").value = time;
               </script>
</form>
</body>
<script>
var today = new Date();
</script>
