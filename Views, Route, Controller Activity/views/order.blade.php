<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Order Details</title>
 
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
<style>
        .topnav {
          background-color: #333;
          overflow: hidden;
        }

        .topnav a {
          float: left;
          color: #f2f2f2;
          text-align: center;
          padding: 14px 16px;
          text-decoration: none;
          font-size: 17px;
        }

        /* Change the color of links on hover */
        .topnav a:hover {
          background-color: #ddd;
          color: black;
        }

        /* Add a color to the active/current link */
        .topnav a.active {
          background-color: #04AA6D;
          color: white;
        }
    </style>
</head>
<script>
    
</script>
<body>
    <center><div class="topnav">
  <a href="{{ url('/item') }}">Item</a>
  <a href="{{ url('/customer') }}">Customer</a>
  <a class="active" href="{{ url('/order') }}">Order</a>
  <a href="{{ url('/orderDetails') }}">Order Details</a>
</div></center>
                          
  <h1><b>Order</b></h1><br>
  <h2 class="masthead-heading  mb-0 "><b>Customer ID:</b>&nbsp&nbsp&nbsp<?php echo $id; ?></h2>
  <h2 class="masthead-heading  mb-0 "><b>Name:</b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<?php echo $name; ?></h2> 
  <h2 class="masthead-heading  mb-0 "><b>Order No:</b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<?php echo $num; ?></h2>
  <h2 class="masthead-heading  mb-0 "><b>Order Date:</b>&nbsp&nbsp&nbsp&nbsp&nbsp<?php echo $date; ?></h2>  
</div>
</div>
</div>
</body>
</html>