<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function display()  
    {  
        return view('customer',['id'=> '001','name'=>'John Paul Pimentel','add'=>'Natividad, Pangasinan']);   
    }
    public function open()  
    {  
        return view('item',['no'=> '101','name'=>'Milktea','price'=>'75']);   
    }
    public function dsply()  
    {  
        return view('order',['id'=> '001','name'=>'John Paul Pimentel','num'=>'1','date'=>'April 27, 2022']);   
    }
    public function opn()  
    {  
        return view('orderDetails',['TransNo'=> '857951','num'=>'1','no'=>'101','name'=>'John Paul Pimentel','price'=>'75', 'qty'=>'3']);   
    }
}
