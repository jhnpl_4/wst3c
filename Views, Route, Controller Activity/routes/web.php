<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/post', [PostsController::class, 'index']);
Route::get('/post/create', [PostsController::class, 'create'])*/;


/*Route::get('/', function () {
    return view('welcome');
});*/

/*Route::get('/example', function () {
    return ('Hello');
});*/

/*Route::view('/','customer');
Route::view('/customer','customer');
Route::view('/item','item');
Route::view('/order', 'order');
Route::view('/orderDetails', 'orderDetails');*/

/*Route::get('customer/{cusid?}/{cusname?}/{cusad?}', function($cusid = '1',$cusname = 'John Paul Pimentel',$cusad = 'Natividad')
{
    return 'customer'.$cusid.$cusname.$cusad;
});

Route::get('item/{itid?}/{itname?}/{itad?}', function($itid = '101',$itname = 'Milktea',$itprice = '75')
{
    return 'item'.$itid.$itname.$itprice;
});*/

/*Route::get(“’shopping/item’,function()  
{  
    $url = route('shopping.item');      
    return  $url;  
})->name('shopping.item');  

Route::get('shopping/customer',function()  
{  
    $url = route('shopping.customer): ;
    return $url;  
})->name('shopping.customer');*/

Route::get('/customer', [OrderController::class, 'display' ]);
Route::get('/', [OrderController::class, 'open' ]);
Route::get('/item', [OrderController::class, 'open' ]);
Route::get('/order', [OrderController::class, 'dsply' ]);
Route::get('/orderDetails', [OrderController::class, 'opn' ]);