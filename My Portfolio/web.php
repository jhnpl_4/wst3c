<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::view('/','activity1');
Route::view('/about me','about me');
Route::view('/contact', 'contact');
Route::view('/loginfrm', 'loginfrm');
Route::view('/registration', 'registration');
Route::view('/activity1', 'activity1');
Route::view('/resume', 'resume');
Route::view('/gallery', 'gallery');