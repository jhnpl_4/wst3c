<!DOCTYPE html>
<html>
<head>
   <title>My Portfolio</title>
   <link rel="icon" href="{!! asset('image/log.png') !!}"/>
   <style>
      body{
   background: #292929;
   display: flex;
   justify-content: center;
   align-items: center;
   height: 100vh;
   flex-direction: column;
}
*{
   font-family: sans-serif;
   box-sizing: border-box;
}

form{
   width: 500px;
   border: 2px solid #ccc;
   padding: 30px;
   background: #fff;
   border-radius: 15px;
}
h2{
   text-align: center;
   margin-bottom: 40px;
}
input{
   display: block;
   border: 2px solid #ccc;
   width: 95%;
   padding: 10px;
   margin: 10px auto;
}
button{
   float: right;
   background: #555;
   padding: 10px 15px;
   color: #fff;
   border-radius: 5px;
   margin-right: 10px;
}

button:hover{
   opacity: .7;
}
   </style>
</head>
<body>
   <form action="/activity1">
        <div class="container">
          <label for="name"><b>Name</b></label>
          <input type="text" placeholder="Enter Name" name="name">

          <label for="psw"><b>Password</b></label>
          <input type="password" placeholder="Enter Password" name="psw">

          <button type="submit">Login</button>
          <label>
            <input type="checkbox" checked="checked" name="remember"> Remember me
          </label>
        </div>

        <div class="container" style="background-color:#f1f1f1">
          <button type="button" class="cancelbtn">Cancel</button>
          <span class="psw">Forgot <a href="#">password?</a></span>
        </div>
   </form>
</body>
</html>