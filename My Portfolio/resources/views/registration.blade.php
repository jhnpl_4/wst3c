<!DOCTYPE html>
<html>
<head>
   <title>My Portfolio</title>
   <link rel="icon" href="{!! asset('image/log.png') !!}"/>
   <style>
      body{
   background: #292929;
   display: flex;
   justify-content: center;
   align-items: center;
   height: 100vh;
   flex-direction: column;
}
*{
   font-family: sans-serif;
   box-sizing: border-box;
}

form{
   width: 500px;
   border: 2px solid #ccc;
   padding: 30px;
   background: #fff;
   border-radius: 15px;
}
h2{
   text-align: center;
   margin-bottom: 40px;
}
input{
   display: block;
   border: 2px solid #ccc;
   width: 95%;
   padding: 10px;
   margin: 10px auto;
}
button{
   float: right;
   background: #555;
   padding: 10px 15px;
   color: #fff;
   border-radius: 5px;
   margin-right: 10px;
}

button:hover{
   opacity: .7;
}
   </style>
</head>
<body>
   <form action="/activity1">
  <div class="container">
    <h1>Register</h1>
    <p>Please fill in this form to create an account.</p>
    <hr>

    <label for="email"><b>Email</b></label>
    <input type="text" placeholder="Enter Email" name="email" id="email">

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" id="psw">

    <label for="psw-repeat"><b>Repeat Password</b></label>
    <input type="password" placeholder="Repeat Password" name="psw-repeat" id="psw-repeat">
    <hr>

    <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>
    <button type="submit" class="registerbtn">Register</button>
  </div>

  <div class="container signin">
    <p>Already have an account? <a href="#">Sign in</a>.</p>
  </div>
</form>
</body>
</html>