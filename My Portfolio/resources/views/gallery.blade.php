<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>My Portfolio</title>
        <!-- Favicon-->
        <link rel="icon" href="{!! asset('image/log.png') !!}"/>
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
        <style>
div.gallery {
  border: 1px solid #ccc;
}

div.gallery:hover {
  border: 1px solid #777;
}

div.gallery img {
  width: 100%;
  height: auto;
}

div.desc {
  padding: 15px;
  text-align: center;
}

* {
  box-sizing: border-box;
}

.responsive {
  padding: 0 6px;
  float: left;
  width: 24.99999%;
}

@media only screen and (max-width: 700px) {
  .responsive {
    width: 49.99999%;
    margin: 6px 0;
  }
}

@media only screen and (max-width: 500px) {
  .responsive {
    width: 100%;
  }
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}
</style>
    </head>
        <body class="d-flex flex-column">
        <main class="flex-shrink-0">
            <!-- Navigation-->
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <div class="container px-5">
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                            <li class="nav-item"><a class="nav-link" href="/activity1">Home</a></li>
                            <li class="nav-item"><a class="nav-link" href="/about me">About Me</a></li>
                            <li class="nav-item"><a class="nav-link" href="/registration">Registration</a></li>
                            <li class="nav-item"><a class="nav-link" href="/loginfrm">Log In</a></li>
                            <li class="nav-item"><a class="nav-link" href="/contact">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
    	<h2>My Gallery</h2>

<div class="responsive">
  <div class="gallery">
      <img src = {{ URL('image/pic3.jpeg') }} height = "200" width = "200">
  </div>
</div>

<div class="responsive">
  <div class="gallery">
      <img src = {{ URL('image/pic4.jpg') }} height = "200" width = "200">
  </div>
</div>

<div class="responsive">
  <div class="gallery">
      <img src = {{ URL('image/pic5.jpg') }} height = "200" width = "200">
  </div>
</div>

<div class="responsive">
  <div class="gallery">
      <img src = {{ URL('image/pic6.jpeg') }} height = "200" width = "200">
  </div>
</div>

<div class="clearfix"></div>

    </body>
</html>