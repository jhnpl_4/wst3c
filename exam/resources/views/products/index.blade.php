@extends('products.layout')
     
@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title></title>
        <!-- Favicon-->
        <link rel="icon" href="images/log.png">
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css2/styles.css" rel="stylesheet" />
        <style>
            

        </style>
    </head>
    <body>
        <div class="d-flex" id="wrapper">
            <!-- Sidebar-->
            <!-- Page content wrapper-->
            <div id="page-content-wrapper">
                <!-- Top navigation-->
                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                        <div class="container-fluid">
                        <h2><a href="/webExamLect">Sales & Inventory System</h2>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ms-auto mt-2 mt-lg-0">
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">My Profile</a>
                                    <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="#">Profile</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="/webExam">Log out</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <!-- Page content--><br>
                <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('products.create') }}"> Create New Service</a>
                <input id="search" type="text" 
                 placeholder="Search here">
            </div>
        </div>

    </div><br>
    
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
     
     <table class="table table-success table-striped">
  <tr>
            <th>No</th>
            <th>Name</th>
            <th>Price</th>
            <th width="280px">Action</th>
        </tr>
        <tbody id="serv">
        @foreach ($products as $product)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ $product->detail }}</td>
            <td>
                <form action="{{ route('products.destroy',$product->id) }}" method="POST">
           
                    <a class="btn btn-primary" href="{{ route('products.edit',$product->id) }}">Edit</a>
     
                    @csrf
                    @method('DELETE')
        
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
</table><br>
<div class="container">
    <h3>Transaction</h3>
<table border="3" align="center" class="table table-striped">
  <tr>
    <th>Product</th>
    <th>Total Sales</th>
  </tr>
  <tr>
    <td>Iphone 11</td>
    <td>4</td>
  </tr>
  <tr>
    <td>Samsung Galaxy A32</td>
    <td>2</td>
  </tr>
</table>
         </div>
         <div class="container">
    <h3>Users</h3>
<table border="3" align="center" class="table table-striped">
  <tr>
    <th>ID</th>
    <th>Username</th>
    <th>Email</th>
    <th>Password</th>
  </tr>
  <tr>
    <td>1</td>
    <td>Michael_Angelo</td>
    <td>michaelangelo@.gmail.com</td>
    <td>qwertyu</td>
  </tr>
  <tr>
    <td>2</td>
    <td>Raphael_02</td>
    <td>raphael@.gmail.com</td>
    <td>asdfg</td>
  </tr>
  <tr>
    <td>3</td>
    <td>Leonardo_21</td>
    <td>leonardo@.gmail.com</td>
    <td>plmjiujhbgf</td>
  </tr>
  <tr>
    <td>3</td>
    <td>Donatello_A5</td>
    <td>donatello@.gmail.com</td>
    <td>qsftgnjktfds</td>
  </tr>
</table>
         </div>
{!! $products->links() !!}
        
@endsection
<script>
            $(document).ready(function() {
                $("#search").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#serv tr").filter(function() {
                        $(this).toggle($(this).text()
                        .toLowerCase().indexOf(value) > -1)
                    });
                });
            });
        </script>
    
    
        </div>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js2/scripts.js"></script>
    </body>
</html>

               