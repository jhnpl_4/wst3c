@extends('products.layout')
     
@section('content')
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<style>
      body{
   background: #9CEED8;
   display: flex;
   justify-content: center;
   align-items: center;
   height: 130vh;
   flex-direction: column;
}
*{
   font-family: sans-serif;
   box-sizing: border-box;
}

form{
   width: 900px;
   border: 2px solid #ccc;
   padding: 30px;
   background: #fff;
   border-radius: 15px;
}
h2{
   text-align: center;
   margin-bottom: 40px;
}
input{
   display: block;
   border: 2px solid #ccc;
   width: 95%;
   padding: 10px;
   margin: 10px auto;
}
button{
   float: right;
   background: #2C987B;
   padding: 10px 15px;
   color: #ffffff;
   border-radius: 5px;
   margin-right: 10px;
}

button:hover{
   background: #51DFB9;
   color: #000000;
}
   </style>
<body>
        
     
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <form action="{{ route('products.update',$product->id) }}" method="POST" enctype="multipart/form-data"> 
        @csrf
        @method('PUT')
     <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Products</h2>
            </div>
            <div class="pull-right">
                <button href="{{ route('products.index') }}"> Back</button>
            </div>
        </div>
    </div>
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" value="{{ $product->name }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Price:</strong>
                    <textarea class="form-control" style="height:150px" name="detail" placeholder="Detail">{{ $product->detail }}</textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" >Submit</button>
            </div>
        </div>
     </div>
    </form>
@endsection
</body>
</html>
