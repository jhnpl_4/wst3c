<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title></title>
        <!-- Favicon-->
        <link rel="icon" href="images/log.png">
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css2/styles.css" rel="stylesheet" />
        <style>
            

        </style>
    </head>
    <body>
        <div class="d-flex" id="wrapper">
            <!-- Sidebar-->
            <!-- Page content wrapper-->
            <div id="page-content-wrapper">
                <!-- Top navigation-->
                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                        <div class="container-fluid">
                        <h2><a href="/webExamLect">Sales & Inventory System</h2>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ms-auto mt-2 mt-lg-0">
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">My Profile</a>
                                    <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="#">Profile</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="/webExam">Log out</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav><br>
         <div class="col-sm-6 col-md-4 ml-auto" style="margin-left: 400px;">
                     <form action="" method="">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <select class="form-control" id="product" name="product" required>
                                            <option>Select Product</option>
                                            <option>Iphone 11   ₱30,000.00</option>
                                            <option>Samsung Galaxy A32  ₱15,000.00</option>
                                            <option>Macbook Pro  ₱70,000.00</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input name="date" id="date" type="date" class="form-control" placeholder="dd/mm/yyyy" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input name="quantity" id="quantity" type="text" class="form-control" placeholder="Quantity" required>
                                    </div>
                                </div>
                                <a class="btn btn-primary" href="/webExamLecture">Buy</a>
                        </form>   
                </div>

