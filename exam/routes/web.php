<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ValidationController;
use App\Http\Controllers\StudInsertController;
use App\Http\Controllers\StudViewController;
use App\Http\Controllers\StudUpdateController;
use App\Http\Controllers\StudDeleteController;
use App\Http\Controllers\CrudActivityController;
use App\Models\Article;
use App\Http\Controllers\ProductController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/','webExam');
Route::view('/webExam','webExam');
Route::view('/webExamLect','webExamLect');
Route::view('/webExamLecture','webExamLecture');
Route::view('/transact','transact');

Route::resource('products', ProductController::class);
Route::get('/webExamLecture', [ProductController::class, 'fetch']);
Route::get('/webExamLect', [ProductController::class, 'fe']);
Route::get('/webExam', [ProductController::class, 'fet']);
Route::get('/', [ProductController::class, 'fetch']);

/*Route::view('/','home');*/
/*Route::view('/home','home');
Route::view('/login','login');
Route::view('/index','index');
Route::view('/register','register');
Route::view('/profile','profile');
Route::view('/gal','gal');
Route::view('/map','map');
  
Route::resource('products', ProductController::class);

Route::get('/home', [ProductController::class, 'fetch']);
Route::get('/', [ProductController::class, 'fetch']);*/
/*Route::view('/','admin');
Route::view('/admin','admin');*/
/*Route::get('/welcome', function () {
    return view('welcome');
});*/