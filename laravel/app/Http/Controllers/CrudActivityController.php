<?php

namespace App\Http\Controllers;
use App\User;
use Crypt;
use Illuminate\Http\Request;
use Illuminate\Contratcs\Encryption\DecryptionException;
use App\Models\user;

class CrudActivityController extends Controller
{
    public function index(){
        return view('crudActivity')
    }

    public function registration(Request $req){
        return view (registration);
        /*$user = new User;
        $user->name = $req->input('name');
        $user->username = $req->input('username');
        $user->email = $req->input('email');
        $user->password = $req->input('password');
        $user->save();
        $req->session()->('user', $req->input('name'))
        return redirect('/');*/
    }

    public function login(Request $req){
        return view (loginfrm);
       /*$user= User::where('email', $req->input('email'))->get();
       if(Crypt::decrypt($user[0]->password)==$req->input('password')){
            $req->session()->('user', $user[0]->name))
        return redirect('/');
       }*/
    }
    public function registerUser(Request $req){
        $request->validate([
             'name'=>'required',
             'username'=>'required',
             'email'=>'required|email|unique:users',
             'password'=>'required|min:6|max:12',
        ]);
        $user = new User();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = $request->password;
        $res = $user->save();
        if($res){
            return back()->with('success', 'You have registered');
        }else{
            return back()->with('fail', 'Something is wrong');
        }
    }
}
