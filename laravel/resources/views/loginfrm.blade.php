<!DOCTYPE html>
<html>
<head>
   <title>My Portfolio</title>
   <link rel="icon" href="{!! asset('image/log.png') !!}"/>
   <style>
      body{
   background: #292929;
   display: flex;
   justify-content: center;
   align-items: center;
   height: 100vh;
   flex-direction: column;
}
*{
   font-family: sans-serif;
   box-sizing: border-box;
}

form{
   width: 500px;
   border: 2px solid #ccc;
   padding: 30px;
   background: #fff;
   border-radius: 15px;
}
h2{
   text-align: center;
   margin-bottom: 40px;
}
input{
   display: block;
   border: 2px solid #ccc;
   width: 95%;
   padding: 10px;
   margin: 10px auto;
}
button{
   float: right;
   background: #e96060;
   padding: 10px 15px;
   color: #ffffff;
   border-radius: 5px;
   margin-right: 10px;
}

button:hover{
   background: #ed1c16;
   color: #000000;
}
   </style>
</head>
<body>
   <form methos="post" action="/appointment">
        <div class="container">
          <label for="username"><b>Username</b></label>
          <input type="text" placeholder="Enter Username" name="username">

          <label for="password"><b>Password</b></label>
          <input type="password" placeholder="Enter Password" name="password">

          <button type="submit">Login</button>
          <label>
            <input type="checkbox" checked="checked" name="remember"> Remember me
          </label>
        </div>

        <div class="container" style="background-color:#f1f1f1">
          <a href="/crudActivity"><button type="button" class="cancelbtn">Cancel</button>
          <span class="password">Forgot <a href="#">password?</a></span>
        </div>
   </form>
</body>
</html>