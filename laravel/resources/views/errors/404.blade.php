<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="error-template">
                <h1>
                    Oops!</h1>
                <h2>
                    404 Not Found</h2>
                <div class="error-details">
                    Agawid kan mali ta innaramid mo
                </div>
                <div class="error-actions">
                    <a href="https://www.youtube.com/" class="btn btn-primary btn-lg">
                        Youtube </a>
                        <a href="https://www.tiktok.com/?lang=en" class="btn btn-default btn-lg">Tiktok</a>
                </div>
            </div>
        </div>
    </div>
</div>