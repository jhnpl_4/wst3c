<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>

    <!-- font icons -->
    <link rel="stylesheet" href="assets/vendors/themify-icons/css/themify-icons.css">
    
    <!-- owl carousel -->
    <link rel="stylesheet" href="assets/vendors/owl-carousel/css/owl.carousel.css">
    <link rel="stylesheet" href="assets/vendors/owl-carousel/css/owl.theme.default.css">

    <!-- Bootstrap + Ollie main styles -->
   <link rel="stylesheet" href="assets/css/ollie.css">
   <style>
      h3{
         color: #e96060;
      }
      button{
           float: right;
           background: #e96060;
           padding: 10px 15px;
           color: #ffffff;
           border-radius: 5px;
           margin-right: 10px;
        }

    button:hover{
       background: #ed1c16;
       color: #000000;
    }
    #customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 2px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #e96060;
  color: white;
}
   </style>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="40" id="home">

    <nav id="scrollspy" class="navbar navbar-light bg-light navbar-expand-lg fixed-top" data-spy="affix" data-offset-top="20">
        <div class="container">  
            <a class="navbar-brand" href="#"><img src="assets/imgs/2.png" alt="" class="brand-img"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/appointment">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#appointment">Appointment</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#booking">Books</a>
                    </li>
                    <li class="nav-item ml-0 ml-lg-4">
                        <a class="nav-link btn btn-primary" href="/crudActivity">Log Out</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <header id="home" class="header">
        <div class="overlay"></div>

        <div id="header-carousel" class="carousel slide carousel-fade" data-ride="carousel">  
            <div class="container">

            <div class="row align-items-center mr-auto">
                <div class="col-md-4">
                    <h3 class="section-title">Pet Care</h3>
                    <p>onsectetur perspiciatis</p>
                </div>
                <div class="col-sm-6 col-md-4 ml-auto">
                    <div class="widget">
                        <div class="infos-wrapper">
                            <h4 class="text-primary">Checkup</h4>
                            <p>onsectetur perspiciatis</p>
                        </div>
                    </div>
                    <div class="widget">
                        <div class="infos-wrapper">
                            <h4 class="text-primary">Emergency</h4>
                            <p>onsectetur perspiciatis</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="widget">
                        <div class="infos-wrapper">
                            <h4 class="text-primary">Care</h4>
                            <p>onsectetur perspiciatis</p>
                        </div>
                    </div>
                    <div class="widget">
                        <div class="infos-wrapper">
                            <h4 class="text-primary">Grooming</h4>
                            <p>onsectetur perspiciatis</p>
                        </div>
                    </div>
                </div>
             </div>
        </div>
    </header>

    <section class="section" id="appointment">

        <div class="container">

            <div class="row align-items-center mr-auto">
                <div class="col-md-4">
                    <h3 class="section-title">Vet Clinic Appointment</h3>
                    <p>Appointment Form</p>
                </div>
                <div class="col-sm-6 col-md-4 ml-auto">
                     <form id="#" class="appoinment-form" method="post" action="#">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <select class="form-control" id="department" name="Service" required>
                                            <option>Select Service</option>
                                            <option>Check-up</option>
                                            <option>Grooming</option>
                                            <option>Care</option>
                                            <option>Emergency</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <select class="form-control" id="pets" name="pet" required>
                                        <option>Select Pet</option>
                                        <option>Dog</option>
                                        <option>Cat</option>
                                        <option>Rabbit</option>
                                        <option>Hamster</option>
                                        <option>Bird</option>
                                        <option>Fish</option>
                                        <option>Snake</option>
                                        <option>Reptiles</option>
                                        <option>Pig</option>
                                        <option>Amphibians</option>
                                            </select>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input name="date" id="date" type="date" class="form-control" placeholder="dd/mm/yyyy" required>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input name="time" id="time" type="time" class="form-control" placeholder="Time" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input name="name" id="name" type="text" class="form-control" placeholder="Full Name" required>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input name="phone" id="phone" type="Number" class="form-control" placeholder="Phone Number" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-2 mb-4">
                                <textarea name="message" id="message" class="form-control" rows="6" placeholder="Your Message"></textarea>
                            </div>
                                <button type="button">Submit</button>
                        </form>   
                </div>
             </div>
        </div>
    </section>

                <section class="section" id="booking">
        <div class="container">

            <div class="row align-items-center mr-auto">
                <div class="col-md-4">
                    <h3 class="section-title">Booking List</h3>
                </div>
                <div class="col-sm-6 col-md-4 ml-auto">
                    <div class="widget">
                        
                    
                </div>
            </div>
        </div>
    </div>
    <table id="customers">
  <tr>
    <th>Pet</th>
    <th>Date</th>
    <th>Time</th>
  </tr>
  <tr>
    <td>Dog</td>
    <td>21/05/2022</td>
    <td>07:15 AM</td>
  </tr>
  <tr>
    <td>Cat</td>
    <td>21/05/2022</td>
    <td>07:15 AM</td>
  </tr>
  <tr>
    <td>Dog</td>
    <td>21/05/2022</td>
    <td>07:15 AM</td>
  </tr>
  <tr>
    <td>Fish</td>
    <td>21/05/2022</td>
    <td>07:15 AM</td>
  </tr>
</table>
    </section>
                <div class="col-md-4 d-none d-md-block order-1">
                    <ul class="list">
                        <li class="list-head">
                            <h6>CONTACT INFO</h6>
                        </li>
                        <li class="list-body">
                            <p class="py-2">Contact us and we'll get back to you within 24 hours.</p>
                            <p class="py-2"><i class="ti-location-pin"></i> 12345 St in your Area</p>
                            <p class="py-2"><i class="ti-email"></i>  info@website.com</p>
                            <p class="py-2"><i class="ti-microphone"></i> (123) 456-7890</p>

                        </li>
                    </ul> 
                </div>
            </div>

        </div>
   
   <!-- core  -->
    <script src="assets/vendors/jquery/jquery-3.4.1.js"></script>
    <script src="assets/vendors/bootstrap/bootstrap.bundle.js"></script>

    <!-- bootstrap 3 affix -->
   <script src="assets/vendors/bootstrap/bootstrap.affix.js"></script>
    
    <!-- Owl carousel  -->
    <script src="assets/vendors/owl-carousel/js/owl.carousel.js"></script>


    <!-- Ollie js -->
    <script src="assets/js/Ollie.js"></script>

</body>
</html>