<!DOCTYPE html>
<html>
<head>
   <title>My Portfolio</title>
   <link rel="icon" href="{!! asset('image/log.png') !!}"/>
   <style>
      body{
   background: #292929;
   display: flex;
   justify-content: center;
   align-items: center;
   height: 100vh;
   flex-direction: column;
}
*{
   font-family: sans-serif;
   box-sizing: border-box;
}

form{
   width: 500px;
   border: 2px solid #ccc;
   padding: 30px;
   background: #fff;
   border-radius: 15px;
}
h2{
   text-align: center;
   margin-bottom: 40px;
}
input{
   display: block;
   border: 2px solid #ccc;
   width: 95%;
   padding: 10px;
   margin: 10px auto;
}
button{
   float: right;
   background: #e96060;
   padding: 10px 15px;
   color: #ffffff;
   border-radius: 5px;
   margin-right: 10px;
}

button:hover{
   background: #ed1c16;
   color: #000000;
}
   </style>
</head>
<body>
   <form action = "\loginfrm" method="post">
  <div class="container">
    <h1>Register</h1>
    <p>Please fill in this form to create an account.</p>
    <hr>
    
    <label for="name"><b>Name</b></label>
    <input type="text" name='name' placeholder="Enter Name" name="name" value="{{old('name')}}" required>
    <span class="text-danger">@error('name'){{$message}} @enderror</span>

    <label for="username"><b>Username</b></label>
    <input type="text" placeholder="Enter username" name="username" value="{{old('username')}}" required>
    <span class="text-danger">@error('username'){{$message}} @enderror</span>

    <label for="email"><b>Email</b></label>
    <input type="text" placeholder="Enter email" name="email" value="{{old('email')}}" required>
    <span class="text-danger">@error('email'){{$message}} @enderror</span>

    <label for="password"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password"value="" required>
    <span class="text-danger">@error('password'){{$message}} @enderror</span>
    <hr>

    <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>
    <button type="submit"><a href = "/loginfrm">Register</a></button>
  </div>

  <div class="container signin">
    <p>Already have an account? <a href="/loginfrm">Sign in</a>.</p>
  </div>
</form>
</body>
</html>