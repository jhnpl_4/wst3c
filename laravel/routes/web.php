<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ValidationController;
use App\Http\Controllers\StudInsertController;
use App\Http\Controllers\StudViewController;
use App\Http\Controllers\StudUpdateController;
use App\Http\Controllers\StudDeleteController;
use App\Http\Controllers\CrudActivityController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/post', [PostsController::class, 'index']);
Route::get('/post/create', [PostsController::class, 'create'])*/;

Route::get('/validation', [ValidationController::class, 'showform']);
Route::post('/validation', [ValidationController::class, 'validateform']);

Route::get('insert', [StudInsertController::class, 'insertform']);
Route::post('create', [StudInsertController::class, 'insert']);

Route::get('view-records',[StudViewController::class, 'index']);

Route::get('edit-records',[StudUpdateController::class, 'index']);
Route::get('edit/{id}',[StudUpdateController::class, 'show']);
Route::post('edit/{id}',[StudUpdateController::class, 'edit']);

Route::get('delete-records',[StudDeleteController::class, 'index']);
Route::get('delete/{id}',[StudDeleteController::class, 'destroy']);

/*Route::get('/', function () {
    return view('welcome');
});*/

/*Route::get('/', function () {
    return view('crudActivity');
});*/
Route::view('/','crudActivity');
Route::view('/loginfrm','loginfrm');
Route::view('/crudActivity','crudActivity');
Route::view('/appointment','appointment');
Route::view('/registration','registration');
/*Route::view('loginfrm',[CrudActivityController::class, 'login']);
Route::view('registration',[CrudActivityController::class, 'registration']);*/

/*Route::view('/register-user',[CrudActivityController::class, 'registerUser'])->name('register-user');*/

/*Route::get('/example', function () {
    return ('Hello');
});*/

/*Route::view('/','customer');
Route::view('/customer','customer');
Route::view('/item','item');
Route::view('/order', 'order');
Route::view('/orderDetails', 'orderDetails');*/

/*Route::get('customer/{cusid?}/{cusname?}/{cusad?}', function($cusid = '1',$cusname = 'John Paul Pimentel',$cusad = 'Natividad')
{
    return 'customer'.$cusid.$cusname.$cusad;
});

Route::get('item/{itid?}/{itname?}/{itad?}', function($itid = '101',$itname = 'Milktea',$itprice = '75')
{
    return 'item'.$itid.$itname.$itprice;
});*/

/*Route::get(“’shopping/item’,function()  
{  
    $url = route('shopping.item');      
    return  $url;  
})->name('shopping.item');  

Route::get('shopping/customer',function()  
{  
    $url = route('shopping.customer): ;
    return $url;  
})->name('shopping.customer');*/

/*Route::get('/customer', [OrderController::class, 'display' ]);
Route::get('/', [OrderController::class, 'open' ]);
Route::get('/item', [OrderController::class, 'open' ]);
Route::get('/order', [OrderController::class, 'dsply' ]);
Route::get('/orderDetails', [OrderController::class, 'opn' ]);*/