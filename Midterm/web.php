<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

/*Route::get('/example', function () {
    return ('Hello');
});*/

Route::view('/','customer');
Route::view('/customer','customer');
Route::view('/item','item');
Route::view('/order', 'order');
Route::view('/orderDetails', 'orderDetails');

/*Route::get('customer/{cusid?}/{cusname?}/{cusad?}', function($cusid = '1',$cusname = 'John Paul Pimentel',$cusad = 'Natividad')
{
    return 'customer'.$cusid.$cusname.$cusad;
});

Route::get('item/{itid?}/{itname?}/{itad?}', function($itid = '101',$itname = 'Milktea',$itprice = '75')
{
    return 'item'.$itid.$itname.$itprice;
});*/